<?php
$property = [
    [
        'title' => 'одноместный номер',
        'type' => 'hotel_room',
        'address' => 'г.Харьков, ул.Жуковского,113',
        'price' => 9.5 . ' usd',
        'description' => 'Одноместный номер с необходимыми удобствами. Идеальный вариант для командировочных.',
        'roomNumber' => 7
    ],
    [
        'title' => 'квартира-студия на Баррикадной',
        'type' => 'apartment',
        'address' => 'г.Днепр, ул.Баррикадная,19',
        'price' => 170 . ' usd',
        'description' => 'Квартира-студия с мебелью и всеми коммуникациями.',
        'kitchen' => true
    ],
    [
        'title' => 'морской коттедж',
        'type' => 'house',
        'address' => 'г.Бердянск, ул.Южная,12',
        'price' => 230 . ' usd',
        'description' => 'Дом с выходом к морю.',
        'roomsAmount' => 3
    ],
    [
        'title' => 'лофт-мечта',
        'type' => 'apartment',
        'address' => 'Dreamcity, Silence street,1',
        'price' => 1 . ' usd',
        'description' => 'Лучшее, что с вами было в жизни!!!',
        'kitchen' => true
    ],
    [
        'title' => 'дом на Киевской',
        'type' => 'house',
        'address' => 'г.Сумы, ул.Киевская,36',
        'price' => 110 . ' usd',
        'description' => 'Частный дом с подземным гаражом',
        'roomsAmount' => 4
    ],
    [
        'title' => 'квартира под аренду',
        'type' => 'apartment',
        'address' => 'г.Веселые княжичи, ул.Боярская,111',
        'price' => 15 . ' usd',
        'description' => 'Однокомнатная квартира с душевой и туалетом, кухня отсуствует.',
        'kitchen' => false
    ],
    [
        'title' => 'двухместный номер',
        'type' => 'hotel_room',
        'address' => 'Lovecity, St.Valentine street,14',
        'price' => 99 . ' usd',
        'description' => 'Номер для молодоженов',
        'roomNumber' => 69
    ],
    [
        'title' => 'семейный номер',
        'type' => 'hotel_room',
        'address' => 'г.Счастье, ул.Благодатная,55',
        'price' => 20 . ' usd',
        'description' => 'Номер для семьи с детьми.',
        'roomNumber' => 33
    ],
    [
        'title' => 'дачный домик',
        'type' => 'house',
        'address' => 'г.Рудное, ул.Садовая,1',
        'price' => 17 . ' usd',
        'description' => 'Участок с домиком на окраине города.',
        'roomsAmount' => 2
    ],
    [
        'title' => 'гостинный дом',
        'type' => 'house',
        'address' => 'г.Вечный, ул.Дорожная,65',
        'price' => 201 . ' usd',
        'description' => 'Частный дом отельного типа для большой компании.',
        'roomsAmount' => 10
    ]
];
?>