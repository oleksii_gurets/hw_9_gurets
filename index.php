<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/my_array.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/HotelRoom_class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Apartment_class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/House_class.php';

$propertyObjects = []; //Создаем массив, где получаем экземпляр нужного класса по типу объекта из массива объектов недвижимости
foreach ($property as $propertyObject) {
    switch ($propertyObject['type']) {
        case 'hotel_room':
            $propertyObjects[] = new HotelRoom($propertyObject['title'], $propertyObject['type'], $propertyObject['address'],
            $propertyObject['price'], $propertyObject['description'], $propertyObject['roomNumber']);
            break;
        case 'apartment':
            $propertyObjects[] = new Apartment($propertyObject['title'], $propertyObject['type'], $propertyObject['address'],
            $propertyObject['price'], $propertyObject['description'], $propertyObject['kitchen']);
            break;
        case 'house':
            $propertyObjects[] = new House($propertyObject['title'], $propertyObject['type'], $propertyObject['address'],
            $propertyObject['price'], $propertyObject['description'], $propertyObject['roomsAmount']);
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Домашнее задание №9 Гурца Алексея</title>
    <meta name="description" content="Введение в ООП">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <header class="p-3 m-3 text-warning" style="background-color: #4b6477;">
            <div class="text-center">
                <h1>Домашнее задание №9</h1>
                <h2>Введение в ООП. Работа с классами и объектами</h2>
            </div>
        </header>
    </div>
    <div class="container">
        <div class="row">
            <table class="table table-warning table-hover table-bordered border-primary">
                <thead>
                    <tr style="text-align: center;">
                        <th scope="col">Название</th>
                        <th scope="col">Тип</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Детальная информация</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($propertyObjects as $key => $propertyObject): ?>
                        <tr style="text-align: center;">
                            <td><?=$propertyObject->title; ?></td>
                            <td><?=$propertyObject->type; ?></td>
                            <td><?=$propertyObject->price; ?></td>
                            <td><a href="/details_info.php?property_id=<?=$key //передаем параметр ключа текущего объекта методом GET?>">Подробнее</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container-fluid">
        <footer class="d-flex justify-content-center p-1 m-3 text-warning" style="background-color: #4b6477;">
            <p>Гурец Алексей &copy;2021 <a href="mailto:oleksii.gurets@gmail.com" class="text-warning">Все вопросы по
                    почте</a> <a href="/" class="text-warning">Главная</a></p>
        </footer>
    </div>

    
</body>

</html>