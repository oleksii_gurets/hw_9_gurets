<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/my_array.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/HotelRoom_class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Apartment_class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/House_class.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/HotelRoom_writer.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Apartment_writer.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/House_writer.php';

if(!empty($_GET['property_id']) || $_GET['property_id'] == 0){ // Проверяем, получили ли мы ключ из строки параметров
    $propertyId = $_GET['property_id'];
}else{
    header('Location:/');
    die();
}
$propertyItem = $property[$propertyId]; // Смотрим, какой объект по типу через массив недвижимости, создаем экземпляр нужного класса с его writer'ом
switch ($propertyItem['type']) {
    case 'hotel_room':
        $propertyObject = new HotelRoom($propertyItem['title'], $propertyItem['type'], $propertyItem['address'],
        $propertyItem['price'], $propertyItem['description'], $propertyItem['roomNumber']);
        $writer = new HotelRoomWriter();
        break;
    case 'apartment':
        $propertyObject = new Apartment($propertyItem['title'], $propertyItem['type'], $propertyItem['address'],
        $propertyItem['price'], $propertyItem['description'], $propertyItem['kitchen']);
        $writer = new ApartmentWriter();
        break;
    case 'house':
        $propertyObject = new House($propertyItem['title'], $propertyItem['type'], $propertyItem['address'],
        $propertyItem['price'], $propertyItem['description'], $propertyItem['roomsAmount']);
        $writer = new HouseWriter();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Детальная информация об объекте</title>
    <meta name="description" content="Введение в ООП">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
    <?=$writer->write($propertyObject); // Выводим информацию на странице "Подробнее" ?>
</body>
</html>
