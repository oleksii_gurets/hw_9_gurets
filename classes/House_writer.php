<?php
class HouseWriter{
    public function write(House $houseItem){
        return $houseItem->getSummaryLine() . '<p class="text-info bg-dark text-wrap text-center">' . $houseItem->description . '</p>';
    }
}
?>