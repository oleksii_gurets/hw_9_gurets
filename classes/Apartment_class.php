<?php
class Apartment{
    public $title = '';
    public $type = '';
    public $address = '';
    public $price = 0.0;
    public $description = '';
    public $kitchen = false;

    public function __construct($title, $type, $address, $price, $description, $kitchen){
        $this->title = $title;
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
        $this->kitchen = $kitchen;
    }

    public function getSummaryLine(){
        if ($this->kitchen == true) {
            $kitchen = 'есть';
        } else {
            $kitchen = 'отсуствует';
        }
        return '<div class="m-3 d-flex justify-content-center">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><strong>Название: </strong>' . $this->title . '</li>
                        <li class="list-group-item"><strong>Тип жилья: </strong>' . $this->type . '</li>
                        <li class="list-group-item"><strong>Адрес: </strong>' . $this->address . '</li>
                        <li class="list-group-item"><strong>Цена суточной аренды: </strong>' . $this->price . '</li>
                        <li class="list-group-item"><strong>Кухня: </strong>' . $kitchen . '</li>
                    </ul>
                </div>
        '; 
    }
}
?>