<?php
class ApartmentWriter{
    public function write(Apartment $apartmentItem){
        return $apartmentItem->getSummaryLine() . '<p class="text-info bg-dark text-wrap text-center">' . $apartmentItem->description . '</p>';
    }
}
?>