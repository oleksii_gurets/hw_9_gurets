<?php
class HotelRoomWriter{
    public function write(HotelRoom $hotelItem){
        return $hotelItem->getSummaryLine() . '<p class="text-info bg-dark text-wrap text-center">' . $hotelItem->description . '</p>';
    }
}
?>