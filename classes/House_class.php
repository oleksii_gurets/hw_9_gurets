<?php
class House{
    public $title = '';
    public $type = '';
    public $address = '';
    public $price = 0.0;
    public $description = '';
    public $roomsAmount = 0;

    public function __construct($title, $type, $address, $price, $description, $roomsAmount){
        $this->title = $title;
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
        $this->roomsAmount = $roomsAmount;
    }

    public function getSummaryLine(){
        return '<div class="m-3 d-flex justify-content-center">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><strong>Название: </strong>' . $this->title . '</li>
                        <li class="list-group-item"><strong>Тип жилья: </strong>' . $this->type . '</li>
                        <li class="list-group-item"><strong>Адрес: </strong>' . $this->address . '</li>
                        <li class="list-group-item"><strong>Цена суточной аренды: </strong>' . $this->price . '</li>
                        <li class="list-group-item"><strong>Количество комнат: </strong>' . $this->roomsAmount . '</li>
                    </ul>
                </div>
        ';    
    }
}
?>